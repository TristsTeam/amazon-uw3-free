EQUIPMENT
UFA 'Recommended Fitness Equipment' - www.ultimatefitnessapp.com/fitness-equipment  
EXERCISES:

Compound Exercise: 
This is a movement that typically incorporates multiple muscles working together. Thus burning more calories.

Isolation Exercise: 
This is a focused movement on one particular body area or muscle.  

Stabilization Exercise: 
These moves consist of the body and various muscles being put under constant tension to support in static hold or contraction positions.  

Resistance Exercise:  
These allow your body and your muscles to work with and against resistance. Things like dumbbells are used to create unique and effective new exercises and exercise variation.

Body Weight Exercises:  
These exercises encourage you to work with your own bodyweight and are superb exercises to use in any workout program
