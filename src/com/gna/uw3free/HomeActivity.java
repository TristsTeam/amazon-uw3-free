package com.gna.uw3free;

import android.content.Intent;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdRegistration;
import com.amazon.device.ads.AdTargetingOptions;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.gna.uw3free.Infrastructure.CommonBaseClass;
import com.gna.uw3free.Infrastructure.WWUCommon;

public class HomeActivity extends CommonBaseClass implements OnClickListener {

	private AdLayout adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.home);
		intializeStates();
		intializeViews();
		keepScreenOn();
        chartboostSessionStart();

		AdRegistration.setAppKey(getApplicationContext(),
				"f529fd9d448f4a45a62a5e3aa709240e");
		this.adView = (AdLayout) findViewById(R.id.adview);
		// AdRegistration.enableTesting(this, true);
		this.adView.loadAd(new AdTargetingOptions());
	}



	private void intializeViews() {

		StateListDrawable moreBtnStates = new StateListDrawable();
		moreBtnStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.main_more_down));
		moreBtnStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.main_more_norm));

		StateListDrawable infoBtnStates = new StateListDrawable();
		infoBtnStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.maininfo_down));
		infoBtnStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.maininfo_norm));

		StateListDrawable workoutAStates = new StateListDrawable();
		workoutAStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.mainworkout_01_down));
		workoutAStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.mainworkout_01_norm));

		StateListDrawable joinUSStates = new StateListDrawable();
		joinUSStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.mainjoin_down));
		joinUSStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.mainjoin_norm));

		StateListDrawable instructionStates = new StateListDrawable();
		instructionStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.maininstructions_down));
		instructionStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.maininstructions_norm));

		StateListDrawable ufaBarStates = new StateListDrawable();
		ufaBarStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.ufabar_down));
		ufaBarStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.ufabar_norm));

		StateListDrawable takeABreakStates = new StateListDrawable();
		takeABreakStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.main_takeabreak_down));
		takeABreakStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.main_takeabreak_norm));

		StateListDrawable shareWebStates = new StateListDrawable();
		shareWebStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.main_web_down));
		shareWebStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.main_web_norm));

		StateListDrawable plusBtnStates = new StateListDrawable();
		plusBtnStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.main_plus_down));
		plusBtnStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.main_plus_norm));

		ImageView moreBtn = (ImageView) findViewById(R.id.moreBtn);
		moreBtn.setBackgroundDrawable(moreBtnStates);
		moreBtn.setOnClickListener(this);

		ImageButton plusBtn = (ImageButton) findViewById(R.id.plusBtn);
		plusBtn.setBackgroundDrawable(plusBtnStates);
		plusBtn.setOnClickListener(this);

		ImageButton shareWebBtn = (ImageButton) findViewById(R.id.shareWebBtn);
		shareWebBtn.setOnClickListener(this);
		shareWebBtn.setBackgroundDrawable(shareWebStates);

		ImageView infoBtn = (ImageView) findViewById(R.id.infoBtn);
		infoBtn.setBackgroundDrawable(infoBtnStates);
		infoBtn.setOnClickListener(this);

		ImageButton ufaBarBtn = (ImageButton) findViewById(R.id.ufaBarBtn);
		ufaBarBtn.setBackgroundDrawable(ufaBarStates);
		ufaBarBtn.setOnClickListener(this);

		ImageView takeABreak = (ImageView) findViewById(R.id.takeABreak);
		takeABreak.setBackgroundDrawable(takeABreakStates);
		takeABreak.setOnClickListener(this);

		emailButton.setOnClickListener(this);
		// musicButton.setOnClickListener(this);
		notesBarBtn.setOnClickListener(this);

		ImageButton workoutARightArrow = (ImageButton) findViewById(R.id.workoutARightArrow);
		ImageButton joinRightArrow = (ImageButton) findViewById(R.id.joinRightArrow);
		ImageButton instructionRightArrow = (ImageButton) findViewById(R.id.instructionRightArrow);

		workoutARightArrow.setOnClickListener(this);
		joinRightArrow.setOnClickListener(this);
		instructionRightArrow.setOnClickListener(this);

		ImageView workoutACLick = (ImageView) findViewById(R.id.workoutACLick);
		ImageView joinUsClick = (ImageView) findViewById(R.id.joinUsImage);
		ImageView instructionClick = (ImageView) findViewById(R.id.instructionImage);
        LinearLayout iceCreamTab=(LinearLayout)findViewById(R.id.iceCreamTab);
        LinearLayout fatLossTab=(LinearLayout)findViewById(R.id.fatlossTab);
		workoutACLick.setBackgroundDrawable(workoutAStates);
		joinUsClick.setBackgroundDrawable(joinUSStates);
		instructionClick.setBackgroundDrawable(instructionStates);

		workoutACLick.setOnClickListener(this);
		joinUsClick.setOnClickListener(this);
		instructionClick.setOnClickListener(this);
        iceCreamTab.setOnClickListener(this);
        fatLossTab.setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if (view.getId() == R.id.moreBtn || view.getId() == R.id.ufaBarBtn) {
			Intent intent = new Intent(this, MoreActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.plusBtn) {
			Intent intent = new Intent(this, ProVersionActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.infoBtn) {
			Intent intent = new Intent(this, InfoActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.emailBarBtn) {
			email();
		} else if (view.getId() == R.id.shareWebBtn) {
			Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
			myWebLink.setData(Uri.parse("http://www.ultimatefitnessapp.com/"));
			startActivity(myWebLink);
		}

		// else if (view.getId() == R.id.musicBarBtn) {
		// Intent intent = new Intent("android.intent.action.MUSIC_PLAYER");
		// startActivity(intent);
		// }
		else if (view.getId() == R.id.workoutACLick
				|| view.getId() == R.id.workoutARightArrow) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 1);
			startActivity(intent);
		} else if (view.getId() == R.id.joinUsImage
				|| view.getId() == R.id.joinRightArrow) {
			Intent intent=new Intent(this,JoinUsDialogActivity.class);
            intent.putExtra("isComingFromSecond",false);
            startActivity(intent);
		} else if (view.getId() == R.id.instructionImage
				|| view.getId() == R.id.instructionRightArrow) {
			Intent intent = new Intent(this, WWUActivity.class);
			intent.putExtra("value", 2);
			startActivity(intent);
		} else if (view.getId() == R.id.notesBarBtn) {
			Intent intent = new Intent(this, NotesActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.takeABreak) {
			if (WWUCommon.getInstance(this).isNetworkAvailable())
				onChartBoostClick();
			else {
				Intent intent = new Intent(this, GenericDialogActivity.class);
				intent.putExtra("message",
						"Internet connection is not available.");
				intent.putExtra("isQuit", false);
				startActivity(intent);
			}
		}else if(view.getId()==R.id.fatlossTab)
        {
            Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
            myWebLink
                    .setData(Uri
                            .parse("http://ufa.getprograde.com/"));
            startActivity(myWebLink);
        }else if(view.getId()==R.id.iceCreamTab)
        {
            Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
            myWebLink
                    .setData(Uri
                            .parse("https://jd102.infusionsoft.com/go/ffo/TEHL7/"));
            startActivity(myWebLink);
        }
	}    @Override
    protected void onStart() {
        super.onStart();

        cb.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        cb.onStop(this);
    }

    @Override
    public void onBackPressed() {
        if (cb.onBackPressed())
            // If a Chartboost view exists, close it and return
            return;
        else
            // If no Chartboost view exists, continue on as normal
            super.onBackPressed();
    }

}
