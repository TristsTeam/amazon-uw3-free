package com.gna.uw3free.Infrastructure;

import com.gna.uw3free.R;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class BaseDisplayExerciseClass extends CommonBaseClass {
	protected String[] exerciseTextArray;
	protected String[] exerciseNameArray;
	protected int[] firstPictureArray;
	protected int[] secondPictureArray;
	protected int[] thirdPictureArray;
	protected int[] imageCount;

	protected ImageView firstImage;
	protected ImageView secondImage;
	protected ImageButton rightArrowBtn;
	protected ImageButton leftArrowBtn;
	protected ImageButton cancelBtn;
	protected TextView exerciseText;
	protected TextView exerciseCount;
	protected TextView exerciseName;
	protected ImageView titleView;
	protected ImageButton infoBtn;

	public void intializeViews() {
		firstImage = (ImageView) findViewById(R.id.firstImage);
		secondImage = (ImageView) findViewById(R.id.secondImage);
		rightArrowBtn = (ImageButton) findViewById(R.id.rightArrowBtn);
		leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		exerciseText = (TextView) findViewById(R.id.exerciseText);
		exerciseCount = (TextView) findViewById(R.id.exerciseCount);
		exerciseName = (TextView) findViewById(R.id.exerciseName);
		titleView = (ImageView) findViewById(R.id.titleView);
		cancelBtn = (ImageButton) findViewById(R.id.cancelBtn);
		infoBtn = (ImageButton) findViewById(R.id.infoBtn);

	}

	public void workoutAExercise() {
		titleView.setBackgroundResource(R.drawable.title01);
		exerciseTextArray = new String[8];
		exerciseNameArray = new String[8];
		firstPictureArray = new int[8];
		secondPictureArray = new int[8];
		thirdPictureArray = new int[8];
		imageCount = new int[8];

		exerciseTextArray[0] = "With feet elevated on a bench or other similar object, perform a strict push up.";
		exerciseTextArray[1] = "Perform a BW Squat, make sure your heels stay on the ground and your legs go parallel.";
		exerciseTextArray[2] = "With hands close together under shoulders, in a push up position.  Begin the move, keeping elbows tight to your side.  Drop to knees if this is too hard.";
		exerciseTextArray[3] = "Begin in a slow jog but quickly get your knees high so every time you raise your legs in the stationary run your knees are coming up parallel.";
		exerciseTextArray[4] = "Standing, lean forward and reach outward with your left hand as your right leg kicks back so you end with your body horizontal, control the balance and then do all the reps on the other side";
		exerciseTextArray[5] = "With any grip you prefer, perform a pull up, squeezing your biceps at the top point and control descent.";
		exerciseTextArray[6] = "On your back, bend one knee and straighten the other leg holding knees close together, now use your leg as an aid to climb it with your hands as you crunch upward and finally reach to tap your toe, return slowly and repeat all reps on this side, then do all reps on the other side.  Don't lower your leg.";
		exerciseTextArray[7] = "Similar to sky pointers except this time have the soles of your feet together,knees split.Keep this position as you then raise your legs, lift your hips, then return to start and repeat.";

		exerciseNameArray[0] = "Elevated Push Ups";
		exerciseNameArray[1] = "BW Squats";
		exerciseNameArray[2] = "Close Grip Push Up";
		exerciseNameArray[3] = "High Knees";
		exerciseNameArray[4] = "Balance Lean";
		exerciseNameArray[5] = "Pull Ups";
		exerciseNameArray[6] = "Leg Climber";
		exerciseNameArray[7] = "Split Frogs";

		firstPictureArray[0] = R.drawable.w1_exercise01_01;
		firstPictureArray[1] = R.drawable.w1_exercise02_01;
		firstPictureArray[2] = R.drawable.w1_exercise03_01;
		firstPictureArray[3] = R.drawable.w1_exercise04_01;
		firstPictureArray[4] = R.drawable.w1_exercise05_01;
		firstPictureArray[5] = R.drawable.w1_exercise06_01;
		firstPictureArray[6] = R.drawable.w1_exercise07_01;
		firstPictureArray[7] = R.drawable.w1_exercise08_01;

		secondPictureArray[0] = R.drawable.w1_exercise01_02;
		secondPictureArray[1] = R.drawable.w1_exercise02_02;
		secondPictureArray[2] = R.drawable.w1_exercise03_02;
		secondPictureArray[3] = R.drawable.w1_exercise04_02;
		secondPictureArray[4] = R.drawable.w1_exercise05_02;
		secondPictureArray[5] = R.drawable.w1_exercise06_02;
		secondPictureArray[6] = R.drawable.w1_exercise07_02;
		secondPictureArray[7] = R.drawable.w1_exercise08_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;
		thirdPictureArray[7] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 2;
		imageCount[6] = 2;
		imageCount[7] = 2;

	}

}
