package com.gna.uw3free.Infrastructure;

import android.app.Activity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;

public class KeepScreenOnBaseClass extends Activity {
    protected Chartboost cb;
	public void keepScreenOn() {
		Window myWindow = getWindow();
		WindowManager.LayoutParams winParams = myWindow.getAttributes();
		winParams.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
		myWindow.setAttributes(winParams);
	}
    public void chartboostSessionStart()
    {

        cb = Chartboost.sharedChartboost();
        String appId = "519b98b816ba479e1b00000b";
        String appSignature = "5828a00e3e127e7157c08a202b1bb7f2d8e31209";
        cb.onCreate(this, appId, appSignature, this.chartBoostDelegate);
        cb.startSession();
        cb.cacheMoreApps();
    }
    private ChartboostDelegate chartBoostDelegate = new ChartboostDelegate() {

        @Override
        public boolean shouldDisplayInterstitial(String location) {
            return true;
        }

        @Override
        public boolean shouldRequestInterstitial(String location) {
            return true;
        }

        @Override
        public void didCacheInterstitial(String location) {
        }

        @Override
        public void didFailToLoadInterstitial(String location) {


        }

        @Override
        public void didDismissInterstitial(String location) {

            cb.cacheInterstitial(location);

        }

        @Override
        public void didCloseInterstitial(String location) {

        }

        @Override
        public void didClickInterstitial(String location) {

        }

        @Override
        public void didShowInterstitial(String location) {
        }

        @Override
        public boolean shouldDisplayLoadingViewForMoreApps() {
            return true;
        }

        @Override
        public boolean shouldRequestMoreApps() {

            return true;
        }

        @Override
        public boolean shouldDisplayMoreApps() {
            return true;
        }

        @Override
        public void didFailToLoadMoreApps() {
        }

        @Override
        public void didCacheMoreApps() {
        }

        @Override
        public void didDismissMoreApps() {
        }

        @Override
        public void didCloseMoreApps() {
        }

        @Override
        public void didClickMoreApps() {
        }

        @Override
        public void didShowMoreApps() {
        }

        @Override
        public boolean shouldRequestInterstitialsInFirstSession() {
            return true;
        }
    };

    public void onChartBoostClick() {
        this.cb.showMoreApps();
    }


}
